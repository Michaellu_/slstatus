{
  description = "Michaellu's custom dwm status bar";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
  };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";

      overlay = final: prev: {
        slstatus = prev.slstatus.overrideAttrs (old: {
          version = "1.0";
          src = builtins.path { path = ./.; name = "slstatus"; };
        });
      };

      slstatus = (import nixpkgs {
        inherit system;
        overlays = [ overlay ];
      }).slstatus;

    in {
      overlays.default = overlay;

      packages.${system}.default = slstatus;
  };
}
